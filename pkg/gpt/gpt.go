package gtp

import (
	"context"
	"fmt"
	gpt3 "github.com/PullRequestInc/go-gpt3"
	"strings"
)

func GetResponse(client gpt3.Client, ctx context.Context, quesiton string) (string, error) {
	var replay string
	err := client.CompletionStreamWithEngine(ctx, gpt3.TextDavinci003Engine, gpt3.CompletionRequest{
		Prompt: []string{
			quesiton,
		},
		MaxTokens:   gpt3.IntPtr(3000),
		Temperature: gpt3.Float32Ptr(0),
	}, func(resp *gpt3.CompletionResponse) {
		fmt.Print(resp.Choices[0].Text)
		replay += resp.Choices[0].Text
	})
	replay = "```text" + strings.Replace(replay, "```", "---", -1) + "\n```"
	return replay, err
}
