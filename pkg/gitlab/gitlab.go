package gitlab

import (
	"fmt"
	"github.com/xanzy/go-gitlab"
)

type Config struct {
	Host  string
	Token string
}

type CommentHook struct {
	ObjectKind       string           `json:"object_kind"`
	EventType        string           `json:"event_type"`
	User             User             `json:"user"`
	ProjectID        int              `json:"project_id"`
	Project          Project          `json:"project"`
	ObjectAttributes ObjectAttributes `json:"object_attributes"`
	Repository       Repository       `json:"repository"`
	Issue            Issue            `json:"issue"`
	ID               string           `json:"id"`
	Ref              string           `json:"ref"`
	Variables        Variables        `json:"variables"`
}
type User struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Username  string `json:"username"`
	AvatarURL string `json:"avatar_url"`
	Email     string `json:"email"`
}
type Project struct {
	ID                int         `json:"id"`
	Name              string      `json:"name"`
	Description       interface{} `json:"description"`
	WebURL            string      `json:"web_url"`
	AvatarURL         interface{} `json:"avatar_url"`
	GitSSHURL         string      `json:"git_ssh_url"`
	GitHTTPURL        string      `json:"git_http_url"`
	Namespace         string      `json:"namespace"`
	VisibilityLevel   int         `json:"visibility_level"`
	PathWithNamespace string      `json:"path_with_namespace"`
	DefaultBranch     string      `json:"default_branch"`
	CiConfigPath      string      `json:"ci_config_path"`
	Homepage          string      `json:"homepage"`
	URL               string      `json:"url"`
	SSHURL            string      `json:"ssh_url"`
	HTTPURL           string      `json:"http_url"`
}
type ObjectAttributes struct {
	Attachment       interface{} `json:"attachment"`
	AuthorID         int         `json:"author_id"`
	ChangePosition   interface{} `json:"change_position"`
	CommitID         interface{} `json:"commit_id"`
	CreatedAt        string      `json:"created_at"`
	DiscussionID     string      `json:"discussion_id"`
	ID               int         `json:"id"`
	LineCode         interface{} `json:"line_code"`
	Note             string      `json:"note"`
	NoteableID       int         `json:"noteable_id"`
	NoteableType     string      `json:"noteable_type"`
	OriginalPosition interface{} `json:"original_position"`
	Position         interface{} `json:"position"`
	ProjectID        int         `json:"project_id"`
	ResolvedAt       interface{} `json:"resolved_at"`
	ResolvedByID     interface{} `json:"resolved_by_id"`
	ResolvedByPush   interface{} `json:"resolved_by_push"`
	StDiff           interface{} `json:"st_diff"`
	System           bool        `json:"system"`
	Type             interface{} `json:"type"`
	UpdatedAt        string      `json:"updated_at"`
	UpdatedByID      interface{} `json:"updated_by_id"`
	Description      string      `json:"description"`
	URL              string      `json:"url"`
}
type Repository struct {
	Name        string      `json:"name"`
	URL         string      `json:"url"`
	Description interface{} `json:"description"`
	Homepage    string      `json:"homepage"`
}
type Issue struct {
	AuthorID            int           `json:"author_id"`
	ClosedAt            interface{}   `json:"closed_at"`
	Confidential        bool          `json:"confidential"`
	CreatedAt           string        `json:"created_at"`
	Description         string        `json:"description"`
	DiscussionLocked    interface{}   `json:"discussion_locked"`
	DueDate             interface{}   `json:"due_date"`
	ID                  int           `json:"id"`
	Iid                 int           `json:"iid"`
	LastEditedAt        interface{}   `json:"last_edited_at"`
	LastEditedByID      interface{}   `json:"last_edited_by_id"`
	MilestoneID         interface{}   `json:"milestone_id"`
	MovedToID           interface{}   `json:"moved_to_id"`
	DuplicatedToID      interface{}   `json:"duplicated_to_id"`
	ProjectID           int           `json:"project_id"`
	RelativePosition    int           `json:"relative_position"`
	StateID             int           `json:"state_id"`
	TimeEstimate        int           `json:"time_estimate"`
	Title               string        `json:"title"`
	UpdatedAt           string        `json:"updated_at"`
	UpdatedByID         interface{}   `json:"updated_by_id"`
	Weight              interface{}   `json:"weight"`
	URL                 string        `json:"url"`
	TotalTimeSpent      int           `json:"total_time_spent"`
	TimeChange          int           `json:"time_change"`
	HumanTotalTimeSpent interface{}   `json:"human_total_time_spent"`
	HumanTimeChange     interface{}   `json:"human_time_change"`
	HumanTimeEstimate   interface{}   `json:"human_time_estimate"`
	AssigneeIds         []interface{} `json:"assignee_ids"`
	AssigneeID          interface{}   `json:"assignee_id"`
	Labels              []interface{} `json:"labels"`
	State               string        `json:"state"`
	Severity            string        `json:"severity"`
}
type Variables struct {
}

func NewGitLabClient(token, host string) (*gitlab.Client, error) {
	urlStr := fmt.Sprintf("%s/api/v4", host)
	client, err := gitlab.NewClient(token, gitlab.WithBaseURL(urlStr))
	if err != nil {
		return nil, err
	}
	return client, nil
}

func (g *Config) GitLabCommentReplay(projectId, issueId int, DiscussionId, discussion string) error {
	client, err := NewGitLabClient(g.Token, g.Host)
	if err != nil {
		return err
	}
	opt := &gitlab.AddIssueDiscussionNoteOptions{
		Body: &discussion,
	}
	_, _, err = client.Discussions.AddIssueDiscussionNote(projectId, issueId, DiscussionId, opt)
	if err != nil {
		return err
	}
	return nil
}
